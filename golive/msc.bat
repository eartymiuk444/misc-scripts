set /p username="username: "
set /p password="password: "
set /p filename="filename: "

docker exec mongo sh -c "exec mongodump -u %username% -p %password% --authenticationDatabase=admin --db=siteMenu --collection=siteMenu --archive" > C:/apps/db-backups/%filename%-siteMenu.archive
docker exec mongo sh -c "exec mongodump -u %username% -p %password% --authenticationDatabase=admin --db=workingPage --collection=workingPage --archive" > C:/apps/db-backups/%filename%-workingPage.archive
docker exec mongo sh -c "exec mongodump -u %username% -p %password% --authenticationDatabase=admin --db=workingCard --collection=workingCard --archive" > C:/apps/db-backups/%filename%-workingCard.archive

docker exec mongo sh -c "exec mkdir dump"
docker cp C:/apps/db-backups/%filename%-siteMenu.archive mongo:/dump/%filename%-siteMenu.archive
docker cp C:/apps/db-backups/%filename%-workingPage.archive mongo:/dump/%filename%-workingPage.archive
docker cp C:/apps/db-backups/%filename%-workingCard.archive mongo:/dump/%filename%-workingCard.archive

docker exec mongo sh -c "exec mongorestore  -u %username% -p %password% --authenticationDatabase=admin --nsFrom='siteMenu.siteMenu' --nsTo='content.siteMenu' --archive=dump/%filename%-siteMenu.archive"
docker exec mongo sh -c "exec mongorestore  -u %username% -p %password% --authenticationDatabase=admin --nsFrom='workingPage.workingPage' --nsTo='content.workingPage' --archive=dump/%filename%-workingPage.archive"
docker exec mongo sh -c "exec mongorestore  -u %username% -p %password% --authenticationDatabase=admin --nsFrom='workingCard.workingCard' --nsTo='content.workingCard' --archive=dump/%filename%-workingCard.archive"
