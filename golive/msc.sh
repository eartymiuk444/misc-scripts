#! /bin/bash

set -x

read -p "username: " username
read -p "password: " password
read -p "filename: " filename

sudo docker exec mongo sh -c "exec mongodump -u $username -p $password --authenticationDatabase=admin --db=siteMenu --collection=siteMenu --archive" > ~/db-backups/"$filename"-siteMenu.archive
sudo docker exec mongo sh -c "exec mongodump -u $username -p $password --authenticationDatabase=admin --db=workingPage --collection=workingPage --archive" > ~/db-backups/"$filename"-workingPage.archive
sudo docker exec mongo sh -c "exec mongodump -u $username -p $password --authenticationDatabase=admin --db=workingCard --collection=workingCard --archive" > ~/db-backups/"$filename"-workingCard.archive

sudo docker exec mongo sh -c "exec mkdir dump"
sudo docker cp ~/db-backups/"$filename"-siteMenu.archive mongo:/dump/"$filename"-siteMenu.archive
sudo docker cp ~/db-backups/"$filename"-workingPage.archive mongo:/dump/"$filename"-workingPage.archive
sudo docker cp ~/db-backups/"$filename"-workingCard.archive mongo:/dump/"$filename"-workingCard.archive

sudo docker exec mongo sh -c "exec mongorestore  -u $username -p $password --authenticationDatabase=admin --nsFrom='siteMenu.siteMenu' --nsTo='content.siteMenu' --archive=dump/$filename-siteMenu.archive"
sudo docker exec mongo sh -c "exec mongorestore  -u $username -p $password --authenticationDatabase=admin --nsFrom='workingPage.workingPage' --nsTo='content.workingPage' --archive=dump/$filename-workingPage.archive"
sudo docker exec mongo sh -c "exec mongorestore  -u $username -p $password --authenticationDatabase=admin --nsFrom='workingCard.workingCard' --nsTo='content.workingCard' --archive=dump/$filename-workingCard.archive"