set /p db="database name: "
set /p username="username: "
set /p password="password: "

docker exec mongo sh -c "exec mongodump -d %db% -u %username% -p %password% --archive --authenticationDatabase=admin" > C:/apps/db-backups/%db%.archive

pause