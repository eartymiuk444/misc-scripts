#! /bin/bash

set -x

read -p "database name: " db
read -p "username: " username
read -p "password: " password

sudo docker exec mongo sh -c "exec mkdir dump"
sudo docker cp ~/db-backups/$db.archive mongo:/dump/$db.archive
sudo docker exec mongo sh -c "exec mongorestore -u $username -p $password --archive=dump/$db.archive --authenticationDatabase=admin"