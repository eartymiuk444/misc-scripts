#! /bin/bash

set -x

read -p "database name: " db
read -p "username: " username
read -p "password: " password

sudo docker exec mongo sh -c "exec mongodump -d $db -u $username -p $password --archive --authenticationDatabase=admin" > ~/db-backups/$db-$(date +"%Y-%m-%d").archive