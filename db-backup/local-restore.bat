set /p db="database name: "
set /p username="username: "
set /p password="password: "

docker exec mongo sh -c "exec mkdir dump"
docker cp C:/apps/db-backups/%db%.archive mongo:/dump/%db%.archive
docker exec mongo sh -c "exec mongorestore -u %username% -p %password% --archive=dump/%db%.archive --authenticationDatabase=admin"

pause