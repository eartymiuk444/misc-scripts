set /p db="database name: "
set /p collection="collection: "
set /p username="username: "
set /p password="password: "
set /p pretty="pretty print (Y/[N]): "

if "%pretty%"=="Y" (
    docker exec mongo sh -c "exec mongoexport --db=%db% --collection=%collection% --pretty -u %username% -p %password% --authenticationDatabase=admin" > C:/apps/db-backups/%collection%-%db%.json
)
if "%pretty%"=="N" (
    docker exec mongo sh -c "exec mongoexport --db=%db% --collection=%collection% -u %username% -p %password% --authenticationDatabase=admin" > C:/apps/db-backups/%collection%-%db%.json
)
pause